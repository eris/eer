{
  outputs = { self, nixpkgs }: {
    devShell.x86_64-linux =
      let
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
	# manually update haunt to 0.2.6  (see also https://github.com/NixOS/nixpkgs/pull/132856/)
	haunt_0_2_6 = pkgs.haunt.overrideAttrs (oldAttrs: rec {
	    pname = "haunt";
	    version = "0.2.6";
	    src = pkgs.fetchurl {
		url = "https://files.dthompson.us/${pname}/${pname}-${version}.tar.gz";
		hash = "sha256-vPKLQ9hDJdimEAXwIBGgRRlefM8/77xFQoI+0J/lkNs=";
	    };

            # Tests are failing, no idea why...
	    doCheck = false;
	    });
      in pkgs.mkShell {

        # this may be something only required on pukkamustard's weird setup.
	LOCALE_ARCHIVE_2_27 = "${pkgs.glibcLocales}/lib/locale/locale-archive";

        buildInputs = [
	  haunt_0_2_6
	  pkgs.pikchr
	  pkgs.guile_3_0
        ];
      };
  };
}

