(use-modules
 (haunt site)
 (haunt artifact)
 (haunt post)
 (haunt reader)

 (commonmark)
 (sxml simple)

 (rnrs io ports)

 (srfi srfi-1)
 (srfi srfi-19)
 (srfi srfi-9))

;; # Layout

(define style
  (call-with-input-file "layout/style.css" get-string-all))

(define logo
  `((svg
     (@ (id "logo")
	(xmlns "http://www.w3.org/2000/svg")
	(version "1.1")
	(viewBox "0 0 32 32"))
     (defs (clipPath (@ (id "inside"))
		     (circle (@ (cx 16) (cy 16) (r 12)))))
     (circle
      (@ (cx 16) (cy 16) (r 14) (fill "goldenrod") (stroke "black")
	 (stroke-width 2)))
     (path
      (@ (id "symbol")
	 (d "M 6,6 A 8,8 0 0,1 6,26 M 16,1 V 31 M 26,6 A 8,8 0 0,0 26,26")
	 (fill "none") (stroke "black") (stroke-width 2)
	 (clip-path "url(#inside)"))))))

(define (layout site title body)
  `((*TOP*
     (*PI* xml "version=\"1.0\"")
     (html
      (@ (xmlns "http://www.w3.org/1999/xhtml")
	 (xml:lang "en")
	 (dir "ltr"))
      (head
       (title ,title)
       (style (@ (type "text/css")) ,style))
      (body ,logo
	    ,@body
	    (footer
	     (span (a (@ (href "https://codeberg.org/eris/eer/")
			 (title "Git repository from which this site is generated")) "Git"))
	     (span (a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/")
			 (title "Content licensed as CC-BY-SA-4.0")) "CC-BY-SA-4.0"))))))))

;; Every EER corresponds to a Haunt post. Some convenient functions
;; for working with posts/eers:

(define (post-title post)
  (post-ref post 'title))

(define (post-synopsis post)
  (post-ref post 'synopsis))

(define (post-status post)
  (post-ref post 'status))

(define (post-id post)
  (post-ref post 'id))

(define (post-uri post)
  (string-append (post-id post) ".xml"))

(define (sort-posts posts)
  (sort posts (lambda (a b)
		(string<? (post-id a) (post-id b)))))

;; # Index page

(define (eer-table site eers)

  (define (eer->tr eer)
    `(tr (td (a (@ (href ,(post-uri eer))) ,(post-title eer)))
	 (td ,(post-synopsis eer))))

  `(table
    (tr (th "Title")
	(th "Synopsis"))
    ,@(map eer->tr eers)))

(define (index-page-builder site eers)
  (serialized-artifact
   "index.html"
   (layout
    site "ERIS Extensions and Recommendations"
    `((h1 ,(site-title site))

      (p "This is a collection of extensions, recommendations and
best-practices related to the " (a (@ (href "http://purl.org/eris"))
"ERIS encoding") ".")

      (p "The extensions and recommendations provide inspiration and
guidelines for using ERIS practically. They are subject to on-going
research and development.")

      (p "The format of these documents is less formal and stable than
the specification of the encoding.")

      (p "Contributions and discussions are very welcome! Get in touch
via " (a (@ (href "https://eris.codeberg.page/#contact")) "the mailing
list or IRC") ".")

      (p "For more information on ERIS see the " (a (@ (href
"https://eris.codeberg.page/")) "project page") ".")

      (h2 "Content")

      ,(eer-table site (sort-posts eers))))

   sxml->xml))


;; # EER reader

(define eer-matcher
  (lambda (file-name)
    (if (equal? (basename file-name) "index.md")
	#t #f)))

(use-modules (sxml transform)
	     (sxml match)
	     (ice-9 popen)
	     (ice-9 regex))


(define pic-pattern (make-regexp ".pic$"))

(define (pic-file? filename)
  (regexp-match?
   (regexp-exec pic-pattern filename)))

;; Modern browsers only parse a XHTML document as XML when the MIME
;; type is set to 'application/xhtml+xml' (via HTTP `Content-type`
;; header). If this is not set, browsers don't use a XML parser,
;; causing XML namespaces to not properly be resolved. As we can not
;; in general guarantee the proper MIME type to be set, we render a
;; "polyglot" HTML/XHTML document. For this SVG elements can not be
;; namespaced using a prefix (e.g. they can not appear as
;; `<svg:svg>`). Following hackery makes sure that SVG elements do not us
;; a namespace prefix:

(define (sxml-unx tree)
  "Remove x: namespaces."
  (pre-post-order
   tree `((*text* . ,(lambda (s . t) t))
	  (*default* . ,(lambda (s . nodes)
			  (let* ((str (symbol->string s))
				 (dropped (if (string-prefix? "x:" str)
					      (string-drop str 2)
					      str))
				 (sym (string->symbol dropped)))
			    `(,sym ,@nodes)))))))

(define* (pic->sxml file #:key alt)
  (let* ((port (open-input-pipe (string-append "pikchr --svg-only " file)))
	 (sxml (xml->sxml port
			  ;; Use 'x' for the SVG namespaces
			  #:namespaces '((x . "http://www.w3.org/2000/svg"))
			  #:trim-whitespace? #t)))

    (sxml-match (sxml-unx sxml) ; remove the x from namespaced tags
		((*TOP* (svg (@ . ,attrs) . ,nodes))
		 `(figure (svg (@ (xmlns "http://www.w3.org/2000/svg")
				  (alt ,alt)
				  ,@attrs)
			       ,@nodes))))))

(define* (embed-pikchr tree #:key path)

  (define (id-handler s . t) `(,s ,@t))

  (define (img-src x)
    (sxml-match x ((img (@ (src ,src)) ,n ...) src)))

  (define (img-alt x)
    (sxml-match x ((img (@ (alt ,alt)) ,n ...) alt)))

  (define (img-handler s attrs)
    (let ((src (img-src `(,s ,attrs))))
      (if (pic-file? src)
	  (begin
	    (format #t "pikchr '~a'~%" src)
	    (pic->sxml (string-append path file-name-separator-string src)
		       #:alt (img-alt `(,s ,attrs))))

	  ;; not a pic file, return as is
	  `(,s ,attrs))))

  (pre-post-order tree
		  `((img *preorder* . ,img-handler)
		    (*text* . ,(lambda (s . t) t))
		    (*default* . ,id-handler))))

(define (read-eer index)
  (let* ((dir (dirname index))
	 (id (basename dir)))
    (format #t "reading EER '~a'~%" id)
    (call-with-input-file index
      (lambda (port)

	(let ((index-metadata (read-metadata-headers port))
	      (sxml (commonmark->sxml port)))
	  (values
	   (cons* `(id . ,id)
		  index-metadata)
	   (embed-pikchr sxml #:path dir)))))))

(define eer-reader
  (make-reader eer-matcher read-eer))

;; # Build individual EERs
;;
;; An EER corresponds to a Haunt post.

(define (eer-builder site posts)
  (define (eer->page post)
    (serialized-artifact
     (post-uri post)
     (layout
      site
      (string-append "EER - " (post-title post))
      `((nav
	 (a (@ (href "index.html")) "back to index"))
	(article
	 (h1 ,(post-title post))
	 ,(post-sxml post))))
     sxml->xml))
  (map eer->page posts))

;; # EER site
;;
;; This defines the entire site and how files are read and written.

(site
 #:title "ERIS Extensions and Recommendations"
 #:domain "https://eris.codeberg.page/eer/"
 #:default-metadata
 '((author . "Endo Renberg")
   (email  . "endo-renberg@posteo.net"))
 #:posts-directory "eer"
 #:readers (list eer-reader)
 #:file-filter (make-file-filter
		'(;; Emacs temporary files
		  "^\\." "^#" "~$"
		  ;; Pikchr files (handled by pikchr-builder)
		  ".pic$"
		  ;; Turtle files (handled by turtle-builder)
		  ".ttl$"))
 #:builders (list index-page-builder
		  eer-builder)
 #:build-directory "public")
