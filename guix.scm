(use-modules
 (guix packages)
 (guix build-system gnu)
 (gnu packages markup)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages pikchr))

(package
 (name "eris-eer")
 (version "0.0.0")
 (source #f)
 (build-system gnu-build-system)
 (native-inputs
  (list guile-3.0 haunt pikchr))
 (synopsis #f)
 (description #f)
 (home-page #f)
 (license #f))
