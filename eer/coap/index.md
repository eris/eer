title: ERIS over CoAP
synopsis: Transport blocks over CoAP
---

ERIS is designed to traverse as many media as possible but in practice block transport should be dominated by only a handful of protocols selected for maximum interoperability. With that goal in mind this document describes a transport layer using the Constrained Application Protocol (CoAP) [[RFC 7252](https://datatracker.ietf.org/doc/rfc7252/)]. CoAP may be transported over UDP [[RFC 7252](https://datatracker.ietf.org/doc/rfc7252/)], stream transports such as TCP or WebSockets [[RFC 8323](https://www.rfc-editor.org/rfc/rfc8323)], Unix sockets, over Delay Tolerant Networks [[Auzias2015](https://sci-hub.se/10.1109/FiCloud.2015.33)] or experimental protocols such as GNUnet CADET.

The protocol described here is only concerned with opaque ERIS blocks and must never transport decoded ERIS content.

## Store URL

The protocol defines a store that provides multiple resources (e.g. block access). A store is identified by an URL [(RFC 3986](https://datatracker.ietf.org/doc/html/rfc3986)] - the **store URL**, resources are mapped to sub-paths of the store URL.

Examples:

- Store URL: `coap://example.net/my-eris-store`
	- URl of the blocks resource: `coap://example.net/my-eris-store/blocks`
- Store URL: `coap+tcp://spam.works/eris/not-spam`
	- URL of blocks resource: `coap+tcp://spam.works/eris/not-spam/blocks`
- Store URL: `coap+tcp://spam.works/eris/another-store`
	- URL of blocks resource: `coap+tcp://spam.works/eris/another-store/blocks`
- Store URL: `coap+ws://jblis.xyz/stores/35bc6e8f-6b01-46f0-ba77-ddd41ae3ee51`
	- URL of blocks resource: `coap+ws://jblis.xyz/stores/35bc6e8f-6b01-46f0-ba77-ddd41ae3ee51/blocks`

A CoAP endpoint can host multiple stores at different paths. The endpoint may use the same underlying storage for all stores. The individual stores can be used to manage specific block pinnings or quotas. The store URL might serve as a capability to post blocks and should in general not be shared indiscrimantely.

Applications that allow user-defined stores should allow stores to be defined by store URLs.

Store URLs can also be used for peer discovery.

### The `.well-known` store

Endpoints may provide an endpoint wide store at the relative URL path `.well-known/eris`.

The `.well-known` store may be a read-only store that does not allow storing of blocks but provides access to all blocks stored by an endpoint.

## Store Resources

### `blocks`

An ERIS CoAP endpoint should at minimum provide a `blocks` resource that can be used to retrieve a stored block using the `GET` verbs.

#### GET

The [GET](https://www.rfc-editor.org/rfc/rfc7252.html#section-5.8.1) verb retrieves a block.

The block reference (Blake2b256 digest) is encoded in the request as an URI-Query option. The block reference may be the digest as 32 octets or a Base32 [[RFC 4648](https://www.rfc-editor.org/info/rfc4648)] encoding of 52 characters. The human readable form must use uppercase characters (Unicode codepoints 0x30-0x39 and 0x41-0x5A). Implementations must support paths in the human-readable form but should default to transmitting the raw encoding.

If the block is available the response code is 2.05 (Content) with a payload containing the block data. The payload must be in raw octets and the content format must be *application/octet-stream* (this is the default if no Content-Format option is present).

Example:

- Client sends a request for a block to the store at `coap://example.net/my-eris-store`:
  - Uri-Host = "example.net"
  - Uri-Path = "my-eris-store"
  - Uri-Path = "blocks"
  - Uri-Query = "H77AGSYKAVTQPUHODJTQA7WZPTWGTTKLRB2GLMF5H53NEKFJ3FUQ"
- Sever retrieves block and responds with 2.05 (Content) with payload containing the block data.

Optionally a [Size1 option](https://www.rfc-editor.org/rfc/rfc7252.html#section-5.10.9) specifying the size of the block may be included in the request. This may be used by the server to optimize block retrieval. For example:

- Client sends a request for a block to the store at `coap://example.net/my-eris-store`:
  - Uri-Host = "example.net"
  - Uri-Path = "my-eris-store"
  - Uri-Path = "blocks"
  - Uri-Query = "H77AGSYKAVTQPUHODJTQA7WZPTWGTTKLRB2GLMF5H53NEKFJ3FUQ"
  - Size1 = 1024
- Sever retrieves block and responds with 2.05 (Content) with payload containing the block data.

Caching ERIS blocks is encouraged and can always be considered "fresh" in the CoAP caching model. This freshness is expressed with a large [Max-Age](https://www.rfc-editor.org/rfc/rfc7252#section-5.10.5) option value. The default Max-Age value is 60 seconds so this option should be set to a large value (maximum of 0xFFFFFFFF, a long time) unless there is a compelling reason not to do so.

#### PUT

The [PUT](https://www.rfc-editor.org/rfc/rfc7252.html#section-10.1.2) verb submits a block. The payload is the block.

If the block was successfully committed to storage or was already present the response code is 2.01 (Created).

Optionally the `Size1` option may be included to indicate the size of the block to be stored.

Stores that do not alow or do not support storing blocks may responds with 4.01 (Unauthorized) or 4.04 (Not Found).

Example:

- Client wants to store a block at the store `coap://example.net/my-eris-store`:
  - Uri-Host = "example.net"
  - Uri-Path = "my-eris-store"
  - Uri-Path = "blocks"
  - Payload = BLOCK-DATA
  - Size1 = 1024
- Sever stores block and responds with 2.01 (Created)

## Bi-directionality of requests

CoAP allows bi-directional requests. A peer that initiates a connection can be both sender and recipient of requests. The terms server and client are relative to requests - a peer that sends a request is the client of the request and the receiving peer is the server of the request.

Peers that initiate a connection should also be able to respond to requests. Peers that accept connections may also make requests. This allows a more peer-to-peer exchange of blocks.

## Security Considerations

In order to prevent passive adversaries from observing block access patterns (see [relevant section in ERIS specification](http://purl.org/eris#name-observing-block-access)) adequate transport layer encryption should be used (or the protocol should be used over distances that can be physically secured).

This protocol does not recommend or define any specific transport layer encryption. Options include DTLS when using UDP (see [Section 9.1 of RFC 7252](https://www.rfc-editor.org/rfc/rfc7252#section-9.1)), TLS (see [Section 9 of RFC 8323](https://www.rfc-editor.org/rfc/rfc8323#section-9)), IPsec [[RFC 5406](https://datatracker.ietf.org/doc/html/rfc5406)], [Tor](https://torproject.org/) or [Yggdrasil](https://yggdrasil-network.github.io/)).
