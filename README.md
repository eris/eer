# ERIS Extensions and Recommendations

This repository contains documentation of extensions, recommendations and best-practices related to [ERIS](https://purl.org/eris).

## Writing and Publishing

EERs are located in `index.md` files contained in the `eer/*` folders.

A XHTML rendering of EERs and an index page can be generated with [Haunt](https://dthompson.us/projects/haunt.html) by running `haunt build`. Use Guix (`guix shell -Df guix.scm`) or Nix (`nix develop`) for a suitable environment.

Everything is published using [Codeberg Pages](https://codeberg.page/), i.e. the `pages` branch of this repository is made available at [https://eris.codeberg.page/eer](https://eris.codeberg.page/eer).

You can checkout the `pages` branch in an other git worktree: `git worktree add ../eer-pages pages` and then render to the pages worktree with `OUT=../eer-pages make`. To publish you will need to commit and push changes in the `pages` branch.

## License

[CC-BY-SA-4.0](./COPYING)
